package com.aixiao.seckill.controller;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aixiao.entity.seckill.Goods;
import com.aixiao.entity.seckill.Order;
import com.aixiao.entity.seckill.User;
import com.aixiao.seckill.service.impl.GoodsServiceImpl;
import com.aixiao.seckill.service.impl.OrderServiceImpl;
import com.aixiao.seckill.service.impl.UserServiceImpl;
import com.aixiao.seckill.thread.MyThread;

import io.protostuff.ProtostuffIOUtil;
import io.protostuff.runtime.RuntimeSchema;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import redis.clients.jedis.Jedis;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:58:57
 * @Descripton
 */

/*@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@MapperScan("com.aixiao.seckill.mapper")
@ComponentScan("com.aixiao")*/
 @RestController
 @Api(value = "GoodsController")
public class GoodsController {

	@Autowired
	private GoodsServiceImpl goodsServiceImpl;
	@Autowired
	private OrderServiceImpl orderServiceImpl;
	@Autowired
	private UserServiceImpl userServiceImpl;

	private RuntimeSchema<Order> schema = RuntimeSchema.createFrom(Order.class);

	Scanner input = new Scanner(System.in);

	@Test
	public void entrance() {
		Long allSize = goodsServiceImpl.getAllSize();
		System.out.println("\n 一共有：" + allSize + "页\n");
		System.out.println("请选择页数：");
		int choose = input.nextInt();
		goodsServiceImpl.queryAll(choose);
		while (true) {
			System.out.println("\n请选择所要购买的商品：");
			int selectId = input.nextInt();
			Goods goods = goodsServiceImpl.getById(selectId);
			if (goods != null) {
				System.out.println("您所购买的商品号：" + selectId + " 金额为：" + goods.getMoney()+" 开始时间："+goods.getStartTime());
				System.out.println("是否确认购买，yes or no?");
				String yorn = input.next();
				if ("yes".equals(yorn)) {
					User user = userServiceImpl.getById(2);
					MyThread myThread = new MyThread(user.getId(), selectId, goods.getMoney(), user.getIphone(),
							orderServiceImpl);
					Thread thread = new Thread(myThread);
					Thread thread1 = new Thread(myThread);
					Thread thread2 = new Thread(myThread);
					Thread thread3 = new Thread(myThread);
					Thread thread4 = new Thread(myThread);
					Thread thread5 = new Thread(myThread);
					Thread thread6 = new Thread(myThread);
					Thread thread7 = new Thread(myThread);
					Thread thread8 = new Thread(myThread);
					Thread thread9 = new Thread(myThread);
					Thread thread10 = new Thread(myThread);
					Thread thread11 = new Thread(myThread);
					Thread thread12 = new Thread(myThread);
					Thread thread13 = new Thread(myThread);
					Thread thread14 = new Thread(myThread);
					Thread thread15 = new Thread(myThread);
					Thread thread16 = new Thread(myThread);
					Thread thread17 = new Thread(myThread);
					Thread thread18 = new Thread(myThread);
					Thread thread19 = new Thread(myThread);
					thread.start();
					thread1.start();
					thread2.start();
					thread3.start();
					thread4.start();
					thread5.start();
					thread6.start();
					thread7.start();
					thread8.start();
					thread9.start();
					thread10.start();
					thread11.start();
					thread12.start();
					thread13.start();
					thread14.start();
					thread15.start();
					thread16.start();
					thread17.start();
					thread18.start();
					thread19.start();
				} else {
					break;
				}
			}
		}
	}

	@GetMapping("queryAll")
	@ApiOperation(value="根据用户编号获取用户姓名", notes="test: 仅1和2有正确返回")
    @ApiImplicitParam(paramType="query", name = "userNumber", value = "用户编号", required = false, dataType = "Integer")
	public List<Map<String,Object>> queryAll(){
		return goodsServiceImpl.queryAll(1);
	}
	
	@Test
	public void redisTest() {
		String key = "seckill:" + 1;
		Jedis jedis = new Jedis("192.168.145.128", 6379);
		jedis.auth("redis666");
		byte[] bytes = jedis.get(key.getBytes());
		if (bytes != null) {
			Order order = schema.newMessage();
			ProtostuffIOUtil.mergeFrom(bytes, order, schema);

		}
		jedis.auth("redis666");
		System.out.println("jedis == " + jedis.get("name"));
	}

	/*
	 * //post登录
	 * 
	 * @RequestMapping(value = "/login",method = RequestMethod.POST) public
	 * String login(Map<String,Object> map){ //添加用户认证信息 Subject subject =
	 * SecurityUtils.getSubject(); UsernamePasswordToken usernamePasswordToken =
	 * new UsernamePasswordToken( map.get("username").toString(),
	 * map.get("password").toString()); //进行验证，这里可以捕获异常，然后返回对应信息
	 * subject.login(usernamePasswordToken); return "login"; }
	 */

	@RequestMapping(value = "/index")
	public String index() {
		return "index";
	}

	// 登出
	@RequestMapping(value = "/logout")
	public String logout() {
		return "logout";
	}

	// 错误页面展示
	@RequestMapping(value = "/error", method = RequestMethod.POST)
	public String error() {
		return "error ok!";
	}

}
