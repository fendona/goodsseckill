package com.aixiao.seckill.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.aixiao.entity.seckill.Goods;
import com.aixiao.seckill.mapper.GoodsMapper;
import com.aixiao.seckill.service.GoodsService;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:26:02
 * @Descripton
 */
@Service
public class GoodsServiceImpl implements GoodsService {

	@Autowired
	private GoodsMapper goodsMapper;
	Scanner input = new Scanner(System.in);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aixiao.seckill.service.OrderService#getById(java.lang.Integer)
	 */
	
	@Cacheable(value="redisSeckill", key="'goodsmap_'+#page")
	public List<Map<String, Object>> queryAll(Integer page) {
		page = page <= 0 ? 1 : page;
		int pageIndex = (page - 1) * 10;
		List<Map<String, Object>> goodsList = goodsMapper.queryAll(pageIndex);
		for (Map<String, Object> map : goodsList) {
			System.out.println("id：" + map.get("id") + " 		name:" + map.get("name") + " 		money：" + map.get("money")
					+ " 		num：" + map.get("num")+"		秒杀开始时间："+map.get("startTime")+"		秒杀结束时间："+map.get("endTime"));
		}
		return goodsList;
	}

	/*
	 * 获取一共多少页数据
	 */
	public Long getAllSize() {
		Long allSize = goodsMapper.getAllSize();  
		long pageSize = Math.floorMod(allSize, 10);
		pageSize = pageSize != 0 ? allSize / (long) 10 + 1 : allSize / (long) 10;
		return pageSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aixiao.seckill.service.GoodsService#getById()
	 */
	
	@Cacheable(value="redisSeckill", key="'goods_'+#id")
	public Goods getById(Integer id) {
		Goods goods = goodsMapper.getById(id);
		if (goods == null) {
			try {
				throw new Exception("商品号："+id+" 不存在，或者库存不足！请重新选择：");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return goods;
	}

}
