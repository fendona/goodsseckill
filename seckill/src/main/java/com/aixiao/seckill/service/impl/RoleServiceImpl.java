package com.aixiao.seckill.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aixiao.entity.seckill.Role;
import com.aixiao.seckill.mapper.RoleMapper;
import com.aixiao.seckill.service.RoleService;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午5:26:02
 * @Descripton
 */
@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleMapper roleMapper;
	Scanner input = new Scanner(System.in);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aixiao.seckill.service.OrderService#getById(java.lang.Integer)
	 */
	public List<Map<String,Object>> queryAll(Integer page) {
		page = page <= 0 ? 1 : page;
		int pageIndex = (page - 1) * 10;
		return  roleMapper.queryAll(pageIndex);
		
	}

	/*
	 * 获取一共多少页数据
	 */
	public Long getAllSize() {
		Long allSize = roleMapper.getAllSize();
		long pageSize = Math.floorMod(allSize, 10);
		pageSize = pageSize != 0 ? allSize / (long) 10 + 1 : allSize / (long) 10;
		return pageSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aixiao.seckill.service.GoodsService#getById()
	 */
	public Role getById(Integer id) {
		Role role = roleMapper.getById(id);
		return role;
	}
	
	public List<Map<String,Object>> getByUserId(Integer userId) {
		return roleMapper.getByUserId(userId);
	}

}
