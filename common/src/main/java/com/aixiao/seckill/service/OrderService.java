package com.aixiao.seckill.service;

import com.aixiao.entity.seckill.Order;

/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午5:26:59
 * @Descripton
 */
public interface OrderService {
	
	public Order getById(Integer id);
	
	public void insertOrder(Integer userId,Integer goodsId,Double money,Long iphone);
}
