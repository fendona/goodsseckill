package com.aixiao.seckill.mapper;

import java.util.Map;

import com.aixiao.entity.seckill.Order;

/**
 * @author fan shi ke 
 * @date 2019年3月20日 下午5:27:59
 * @Descripton
 */
public interface OrderMapper {
	
	public int insertOrder(Order order);
	
	public Order getById(Integer id);
	
	public void executeSeckillPro(Map<String ,Object> paramMap);
}
