
package com.aixiao.entity.seckill;

/**
 * @author fan shi ke
 * @date 2019年3月20日 下午4:44:40
 * @Descripton
 */
public class User {

	private Integer id;
	private String name;
	private Long iphone;
	private String passWord;
	private String stutas;

	public User() {
	}

	public User(String name, Long iphone, String passWord, String stutas) {
		super();
		this.passWord = passWord;
		this.name = name;
		this.iphone = iphone;
		this.stutas = stutas;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the iphone
	 */
	public Long getIphone() {
		return iphone;
	}

	/**
	 * @param iphone
	 *            the iphone to set
	 */
	public void setIphone(Long iphone) {
		this.iphone = iphone;
	}

	/**
	 * @return the password
	 */

	/**
	 * @return the stutas
	 */
	public String getStutas() {
		return stutas;
	}

	

	/**
	 * @return the passWord
	 */
	public String getPassWord() {
		return passWord;
	}

	/**
	 * @param passWord the passWord to set
	 */
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	/**
	 * @param stutas
	 *            the stutas to set
	 */
	public void setStutas(String stutas) {
		this.stutas = stutas;
	}

}
