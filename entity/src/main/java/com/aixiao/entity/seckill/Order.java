
package com.aixiao.entity.seckill;

import java.sql.Timestamp;

/**
 * @author fan shi ke 
 * @create_at 2019年3月20日 下午5:01:47
 * @Descripton
 */
public class Order {
	
	private Integer id;
	private Integer goodsId;
	private Integer userId;
	private Double money;
	private Long iphone;
	private Timestamp create_at;
	public Order() {
		super();
	}
	public Order(Integer goodsId, Integer userId, Double money, Long iphone, Timestamp create_at) {
		super();
		this.goodsId = goodsId;
		this.userId = userId;
		this.money = money;
		this.iphone = iphone;
		this.create_at = create_at;
	}
	
	public Order(Integer userId) {
		super();
		this.userId = userId;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the goodsId
	 */
	public Integer getGoodsId() {
		return goodsId;
	}
	/**
	 * @param goodsId the goodsId to set
	 */
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the money
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * @param money the money to set
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * @return the iphone
	 */
	public Long getIphone() {
		return iphone;
	}
	/**
	 * @param iphone the iphone to set
	 */
	public void setIphone(Long iphone) {
		this.iphone = iphone;
	}
	/**
	 * @return the create_at
	 */
	public Timestamp getCreate_at() {
		return create_at;
	}
	/**
	 * @param create_at the create_at to set
	 */
	public void setCreate_at(Timestamp create_at) {
		this.create_at = create_at;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Order [id=" + id + ", goodsId=" + goodsId + ", userId=" + userId + ", money=" + money + ", iphone="
				+ iphone + ", create_at=" + create_at + "]";
	}
	
	
	
	
}
