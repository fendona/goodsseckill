package com.aixiao.entity.seckill;

/**
 * @author fan shi ke
 * @date 2019年3月25日 下午3:58:49
 * @Descripton
 */
public class Permission {

	private Long id;
	private String permission;
	private Integer roleId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	/**
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	
}
